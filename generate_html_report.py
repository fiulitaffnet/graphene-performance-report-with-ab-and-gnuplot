from testing_queries import queries
import sys, os, shutil

try:
    concurrency = sys.argv[1]
    requests = sys.argv[2]
except:
    print("Usage: python3 tests/generate_html_report.py [CONCURRENCY] [REQUESTS]")
    sys.exit()

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

report_filepath = os.path.join(__location__, 'report/stress_tests_report_c_' + concurrency + '_n_' + requests + '.html')

with open(report_filepath, 'w' ) as report_file:
    report_file.write("<!DOCTYPE html>\n")
    report_file.write("<html><head><title>Stress Tests Report</title></head>\n")
    report_file.write("<body>\n")
    report_file.write("<h1>Report Stress Tests Hub Finanziario</h1>\n")
    report_file.write("<div>\n")

for q in queries:

    shutil.copy( os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + '.svg'), os.path.join(__location__, 'report/images/' + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + '.svg'))


    html_filepath = os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + '.html')
    
    with open(html_filepath, 'r' ) as html_file:
        partial = html_file.read()

    with open( os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '.json'), 'r' ) as json_file:
        partial_json = json_file.read()

    with open(report_filepath, 'a' ) as report_file:
        report_file.write("<div style='float:left;width:50%'>\n")
        report_file.write("<h1 style='font-family:monospace'>" + q['endpoint'] +"</h1>\n")
        report_file.write("<code style='display:inline-block;white-space: normal;max-width:50%;word-break:break-all;word-wrap:break-word;min-height:100px'>" + partial_json + "</code><br/>\n")
        report_file.write("<img src='./images/" + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + ".svg' /><br/>\n")
        report_file.write(partial + "\n")
        report_file.write("<hr/>\n")
        report_file.write("</div>\n")
        

with open(report_filepath, 'a' ) as report_file:
    report_file.write("<div style='clear:both'></div>\n")
    report_file.write("</div>\n")
    report_file.write("</body></html>")