from testing_queries import queries
import sys, os, shutil

try:
    endpoint = sys.argv[1]
    token = sys.argv[2]
    concurrency = sys.argv[3]
    requests = sys.argv[4]
except:
    print("Usage: python3 tests/generate_stress_tests.py [GRAPHQL_ENDPOINT] [TOKEN] [CONCURRENCY] [REQUESTS]")
    sys.exit()

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

o = '''{ '''
c = ''' }'''

bash_stress_tests_filepath = os.path.join(__location__, 'stress_tests_queries/stress_tests_c_' + concurrency + '_n_' + requests + '.sh')
gnuplot_filepath = os.path.join(__location__, 'stress_tests_queries/stress_tests_c_' + concurrency + '_n_' + requests + '.gnuplot')

with open(bash_stress_tests_filepath, 'w' ) as bash_stress_tests_file:  

    bash_stress_tests_file.write("#!/usr/bin/env bash\n\n")

with open(gnuplot_filepath, 'w' ) as gnuplot_file:

    gnuplot_file.write("set terminal svg size 400,300 enhanced font 'arial,10' butt solid\n")
    gnuplot_file.write("set size 1,1\n")
    gnuplot_file.write("set grid y\n")
    gnuplot_file.write("set xlabel 'richieste'\n")
    gnuplot_file.write("set ylabel 'tempo di risposta (ms)'\n")

for q in queries:
    with open(os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '.json'), 'w' ) as test_file:
        query = '''{ "query": "''' + o + q['endpoint'] + ( ( "(" + q['param_string'] +  ")" ) if q['param_string'] != '' else '') + o 
        for f in q['fields']:
            query = query + f + " "
            if 'subselections' in q:
                if f in q['subselections']:
                    query = query + o + q['subselections'][f] + c
        query = query + c + c + '''" }'''

        test_file.write(query)

    with open(bash_stress_tests_filepath, 'a' ) as bash_stress_tests_file:

        bash_stress_tests_file.write('# ' + q['endpoint'] + '\n')
        bash_stress_tests_file.write('echo "";echo "* ' + q['endpoint'] + ' *";echo "";\n')
        html_filepath = os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + '.html')
        line = '''ab -c ''' + concurrency + ''' -n ''' + requests + ''' -T application/json -p ''' + os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '.json') + ''' -H "Authorization: Bearer ''' + token + '''" -g ''' + os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + '.gnuplot.txt') + ''' ''' + endpoint + ''' > ''' + html_filepath
        bash_stress_tests_file.write(line + '\n')
        line = """sed -i '1,7d' """ + html_filepath + """; sed -i "s/$/<\/pre>/;s/^/<pre>/" """ + html_filepath
        bash_stress_tests_file.write(line + '\n\n')

    with open(gnuplot_filepath, 'a' ) as gnuplot_file:

        gnuplot_file.write("\nset output '" + os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + '.svg') + "'\n")
        gnuplot_file.write("set title '" + q['endpoint'].replace("_"," ") + " | ab -c " + concurrency + " -n " + requests + "'\n")
        gnuplot_file.write("plot '" + os.path.join(__location__, 'stress_tests_queries/' + q['endpoint'] + '_c_' + concurrency + '_n_' + requests + '.gnuplot.txt') + "' using 9 smooth sbezier with lines title ''\n")
