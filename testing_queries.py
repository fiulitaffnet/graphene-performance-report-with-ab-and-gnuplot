# esempio testing_queries.py
        
queries = [

    {
        # Nome della query GraphQL da interrogare
        'endpoint':'nome_query1',

        # Parametri da passare alla query
        'param_string':'nome_param1:valore_param1,nome_param2:valore_param2',

        # Campi che si vogliono ottenere in risposta
        'fields': ['campo1','campo2','campo3'],

        # Sottoselezioni dei campi, se previste
        'subselections': {
            'campo1': 'sottoselezione1 sottoselezione2 sottoselezione3'
        },

        # Campi in subselections che restituiscono valori multipli
        'multiple_values': ['campo1'] # Indica che "campo1" ha valori multipli
        
    },
    {
        'endpoint':'nome_query2',
        'param_string':'nome_parametro1:valore_parametro1',
        'fields': ['campo1','campo2','campo3','campo4'],

        # is_array=True Indica che la query restituisce un array
        'is_array': True 
    },
]